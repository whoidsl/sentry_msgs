# sentry_msgs

This is a catch-all package for sentry-specific message types.  
This package should contain only ROS ```msg``` and ```srv``` files.

All messages contained within should be just for Sentry.  
Messages for extra cool stuff like fancy navigation
or image interpretation or whatever should live elsewhere.

## Contents of this Package

* Uniquely Sentry stuff... not sure yet

## Getting Started

This is a DSL ROS package intended to be run as part of the larger DSL ROS echosystem.
See the [Sentry Wiki](http://sentry-wiki.whoi.edu/ROS_Upgrade)

### Prerequisites

This package requires ds_msgs and a few standard things.

### Installing

You should generally be installing this as part of a rosinstall file.  
If handled seperately though, simply add to a catkin workspace and 
build with:

```
catkin_make
```

## Deployment

Add to a live system and build.  If the system compiles, it will likely work.
But you should probably run a mission in simulation to make sure.

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Ian Vaughn** - *Initial work* - [WHOI email](mailto:ivaughn@whoi.edu)
* **Stefano Suman** - *Initial work* - [WHOI email](mailto:ssuman@whoi.edu)

## Acknowledgments

* IFREMER for their architectural support
* Louis Whitcomb et. al. for his message definitions to look over


